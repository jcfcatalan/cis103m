<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Applications</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="resources/tableExport.js"></script>
<script src="resources/jquery.base64.js"></script>

<script src="resources/jspdf/libs/sprintf.js"></script>
<script src="resources/jspdf/jspdf.js"></script>
<script src="resources/jspdf/libs/base64.js"></script>


<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/accordion.css">

</head>
<script type="text/javascript">
$(document).ready(function() {
	//$('#content').load('views/subscriber/dashboard.jsp');


/* 	$('#tblSubscribers').DataTable( {
        dom: 'Bfrtip',
        ajax: '/WebAppProject/subscribers',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
 */
	$.ajax({
		type: 'GET',
		url: '/WebAppProject/applications',
		contentType: 'application/json',
		success: function(response){
			console.log(response)
			fillTable(response,'#tblBilling');
		}
	});  
	
});

function deleteSubscriber(id){
	
}

function fillTable(response, id){
	console.log(response);
	var trHTML = '';
    $.each(response.data, function (i, item) {
        trHTML += '<tr><td>' + item.subscriberId + '</td><td>' + item.accountNo + '</td><td>' + item.requestId + '</td>';
        trHTML += '<td>' + item.description + '</td><td>' + item.requestedDate + '</td>';
        trHTML += '<td>' + item.status + ' ' + item.street + ' ' + item.city + ' ' + item.province +'</td>';
        trHTML += '<td>' + item.zipCode + '</td>';
        trHTML += '<td>&ensp;<input type="button" id="deleteBtn" value="Remove" onClick="deleteSubscriber(${sub.id})" />' +
			'&ensp;<input type="button" id="updateBtn" value="Update" onClick="getRowValues(${sub.id}, this)" /></td></tr>';
    });
    $(id).append(trHTML);
}


</script>
<!-- <button onclick="$('#tblSubscribers').tableExport({type:'pdf',escape:'false'});">
            Get as Excel spreadsheet
        </button> -->
<body>
	<table id="tblBilling" name="tblBilling">
		<thead>
			<tr>
				<th style="display:none;">Subscriber ID</th>
				<th >Account No</th>
				<th style="display:none;">Request ID</th>
				<th style="display:none;">Description</th>
				<th>Requested Date</th>
				<th>Status</th>
				<th>Street</th>
				<th>City</th>
				<th>Province</th>
				<th>Zip Code</th>
				<th>Action</th>
			</tr>
        </thead>
        <%-- <tbody>
			<c:forEach items="${data}" var="sub">
				<tr>
					<td>${sub.id}</td>
					<td>${sub.accountNo}</td>
					<td>${sub.lastName}</td>
					<td>${sub.firstName}</td>
					<td>${sub.middleName}</td>
					<td>${sub.mobileNo}</td>
					<td>${sub.street}</td>
					<td>${sub.accountStatus}</td>
					<td>&ensp;<input type="button" id="deleteBtn" value="Remove" onClick="deleteSubscriber(${sub.id})" /> 
					&ensp;<input type="button" id="updateBtn" value="Update" onClick="getRowValues(${sub.id}, this)" /></td>
				</tr>
			</c:forEach>
		</tbody> --%>
	</table>
	
</body>

</html>