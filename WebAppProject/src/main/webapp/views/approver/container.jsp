<%@page import="com.project.webapp.model.PostpaidPlan"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Approver</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" >
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/accordion.css" >
</head>

<script type="text/javascript">
function createApplication(){
	$('#content').load('views/approver/addSubscriber.jsp');
}

function viewProfile(){
	$('#content').load('views/approver/profile.jsp');
}

function viewApplicationRequests(){
	$('#content').load('views/approver/viewApplicationRequest.jsp');
}

function viewBillingRequests(){
	$('#content').load('views/approver/viewBillingRequest.jsp');
}

function viewCreditLimitRequests(){
	$('#content').load('views/approver/viewCreditRequest.jsp');
}

function viewSubscriberReport(){
	$('#content').load('views/approver/subscriberList.jsp');
}

function viewRequestReport(){
	$('#content').load('views/approver/report.jsp');
}

function viewApplicationApprovalsReport(){
	$('#content').load('views/approver/approvalReport.jsp');
}

function viewChangeRequestReport(){
	$('#content').load('views/approver/changeRequestReport.jsp');
}
</script>

<body>

<div class="flex-container">
	<header></header>
	
	<div class="nav">
		<button class="accordion">Account</button>
		<div class="panel">
			<button id="" class="acc-sib" onClick="createApplication()">Create Application</button>
			<button id="approverProfile" class="acc-sib" onClick="viewProfile()">Profile</button>
		</div>
		<button class="accordion">Request</button>
		<div class="panel">
			<button id="" class="acc-sib" onClick="viewApplicationRequests()">Application</button>
			<button id="" class="acc-sib" onClick="viewBillingRequests()">Billing Address</button>
			<button id="" class="acc-sib" onClick="viewCreditLimitRequests()">Credit Limit</button>
		</div>
		<button class="accordion">Report</button>
		<div class="panel">
			<button id="" class="acc-sib" onClick="viewSubscriberReport()">Subscriber</button>
			<button id="" class="acc-sib" onClick="viewApplicationApprovalsReport()">Application Approvals</button>
			<button id="" class="acc-sib" onClick="viewChangeRequestReport()">Change Request</button>
		</div>
		<button class="accordion-last">Log Out</button>
		
		<script>
			var accordion = document.getElementsByClassName("accordion");
			for(var i=0; i<accordion.length; i++){
				accordion[i].onclick = function(){
					this.classList.toggle("active");
					this.nextElementSibling.classList.toggle("show");
				}
			}
		</script>
	</div>
	
	<div class="content" id="content">
		<%
    		session.setAttribute("approverId", request.getAttribute("approverId"));
			session.setAttribute("approverName", request.getAttribute("approverName"));
			session.setAttribute("approverEmail", request.getAttribute("approverEmail"));
			session.setAttribute("subscribers", request.getAttribute("subscribers"));
        %>
	</div>
	
	<footer>&copy; 2016</footer>
</div>

</body>
</html>