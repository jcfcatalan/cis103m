<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="resources/tableExport.js"></script>
<script src="resources/jquery.base64.js"></script>

<script src="resources/jspdf/libs/sprintf.js"></script>
<script src="resources/jspdf/jspdf.js"></script>
<script src="resources/jspdf/libs/base64.js"></script>


<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/accordion.css">

</head>

<script type="text/javascript">
$(document).ready(function() {
	//$('#content').load('views/subscriber/dashboard.jsp');


/* 	$('#tblSubscribers').DataTable( {
        dom: 'Bfrtip',
        ajax: '/WebAppProject/subscribers',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
 */
	$.ajax({
			type: 'GET',
			url: '/WebAppProject/subscribers',
			contentType: 'application/json',
			success: function(response){
				console.log(response)
				fillTable(response,'#tblSubscribers');
			}
		});  
	
});

function deleteSubscriber(id){
	
}

function fillTable(response, id){
	console.log(response);
	var trHTML = '';
    $.each(response.data, function (i, item) {
        trHTML += '<tr><td>' + item.id + '</td><td>' + item.accountNo + '</td><td>' + item.lastName + '</td>';
        trHTML += '<td>' + item.firstName + '</td><td>' + item.middleName + '</td>';
        trHTML += '<td>' + item.mobileNo + '</td><td>' + item.street + ' ' + item.city + ' ' + item.province + ' ' + item.zipCode +'</td>';
        trHTML += '<td>' + item.accountStatus + '</td>' + '<td>' + item.approver+ '</td><td>' + item.planDescription + '</td></tr>';
    });
    $(id).append(trHTML);
}



</script>
<!-- <button onclick="$('#tblSubscribers').tableExport({type:'pdf',escape:'false'});">
            Get as Excel spreadsheet
        </button> -->
<body>
	<table id="tblSubscribers" name="tblSubscribers">
		<thead>
			<tr>
				<th>ID</th>
				<th>Account No</th>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Mobile No</th>
				<th>Billing Address</th>
				<th>Account Status</th>
				<th>Approver</th>
				<th>Plan</th>
			</tr>
        </thead>
	</table>
	
</body>
</html>