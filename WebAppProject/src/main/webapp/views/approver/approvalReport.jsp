<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Approvals</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/accordion.css">

</head>
<script type="text/javascript">
$(document).ready(function() {
	//$('#content').load('views/subscriber/dashboard.jsp');


/* 	$('#tblSubscribers').DataTable( {
        dom: 'Bfrtip',
        ajax: '/WebAppProject/subscribers',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
 */
	$.ajax({
			type: 'GET',
			url: '/WebAppProject/approvalcount',
			contentType: 'application/json',
			success: function(response){
				console.log(response)
				fillTable(response,'#tblSubscribers');
			}
		});  
	
});

function deleteSubscriber(id){
	
}

function fillTable(response, id){
	console.log(response);
	var trHTML = '';
    $.each(response.data, function (i, item) {
        trHTML += '<tr><td>' + item.id + '</td><td>' + item.lastName + '</td>';
        trHTML += '<td>' + item.firstName + '</td>'+ '<td>' + item.middleName + '</td> <td>' + item.approvalCount + '</td></tr>';
    });
    $(id).append(trHTML);
}


</script>
<!-- <button onclick="$('#tblSubscribers').tableExport({type:'pdf',escape:'false'});">
            Get as Excel spreadsheet
        </button> -->
<body>
	<table id="tblSubscribers" name="tblSubscribers">
		<thead>
			<tr>
				<th>STAFF ID</th>
				<th>LAST NAME</th>
				<th>FIRST NAME</th>
				<th>MIDDLE NAME</th>
				<th>COUNT</th>
			</tr>
        </thead>
	</table>
	
</body>

</html>