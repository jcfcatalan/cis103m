<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<style>
.success_l {
	color: green;
	font-weight: bold;
}
</style>
</head>

<script>
$(document).ready(function() {
    $('.key-numeric').keypress(function(e) {
            var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
            if (verified) {e.preventDefault();}
    });
    
    $.ajax({
		type: 'GET',
		url: '/WebAppProject/plans',
		contentType: 'application/json',
		success: function(response){
	        console.log(response);
	        $.each(response, function(i, value) {  
	              $('<option></option>', {html:value.description}).attr('value', value.id).appendTo('#planselect');
	           });
		}
	});
   
});
function addSubscriber(){
	var subscriberTable = document.getElementById('subscriberDetails');
	var subscriberDetails = {
		firstName		: $('#firstName').val(),
		middleName		: $('#middleName').val(),
		lastName		: $('#lastName').val(),
		mobileNo		: $('#mobileNo').val(),
		officeNo		: $('#officeNo').val(),
		telephoneNo		: $('#telephoneNo').val(),
		emailAddress	: $('#emailAddress').val(),
		street			: $('#street').val(),
		city			: $('#city').val(),
		province		: $('#province').val(),
		zipCode			: $('#zipCode').val(),
		planId			: $('#planselect').val()
	};
	
	console.log('DETAILS : ', subscriberDetails);
	$.ajax({
		type: 'POST',
		url: '/WebAppProject/add',
		data: JSON.stringify(subscriberDetails),
		contentType: 'application/json',
		success: function(response){
	        console.log(response);
	        $("#responseMessage").text(response.message);
	        $('.subscriberDetailsClass input[type="text"]').val('');
		}
	});
}
</script>

<body>

<table id="subscriberDetails" class="subscriberDetailsClass" >
<tr>
	<td colspan="2"><label id = "responseMessage" class="success_l"></label></td>
</tr>
<tr>
	<td>First Name : </td> 
	<td><input type="text" id="firstName" /></td>
</tr>
<tr>
	<td>Middle Name : </td> 
	<td><input type="text" id="middleName" /></td>
</tr>
<tr>
	<td>Last Name : </td> 
	<td><input type="text" id="lastName" /></td>
</tr>
<tr>
	<td>Mobile Number : </td> 
	<td><input type="text" id="mobileNo" /></td>
</tr>
<tr>
	<td>Office Number : </td> 
	<td><input type="text" id="officeNo" /></td>
</tr>
<tr>
	<td>Telephone Number : </td> 
	<td><input type="text" id="telephoneNo" /></td>
</tr>
<tr>
	<td>Email Address : </td> 
	<td><input type="text" id="emailAddress" /></td>
</tr>
<tr>
	<td>Street : </td> 
	<td><input type="text" id="street" /></td>
</tr>
<tr>
	<td>City : </td> 
	<td><input type="text" id="city" /></td>
</tr>
<tr>
	<td>Province : </td> 
	<td><input type="text" id="province" /></td>
</tr>
<tr>
	<td>Zip Code : </td> 
	<td><input type="text" id="zipCode" class="key-numeric" /></td>
</tr>
<tr>
	<td>Plan : </td> 
	<td><select name="planselect" class="span12" id="planselect" > 
            </select>   </td>
</tr>
<tr>
	<td></td> 
	<td><input type="button" value="Add Subscriber" onClick="addSubscriber()"/></td>
</tr>
</table>

</body>
</html>