<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Credit Limit</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<%
	String msg = (String) request.getAttribute("message");
	if (msg == null) {
		msg = "";
	}
%>
<body>
	<label><%=msg%></label>
	
	<script>
	$(document).ready(function() {
	    $('.key-numeric').keypress(function(e) {
	    	var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
	        if (verified) {
	        	e.preventDefault();
	        }
	    });
	});
	
	function submitRequest(){
		var param = {
			newCreditLimit	: $('#creditLimit').val(),
			mobileNo		: $('#mobileNo').val(),
			requestType		: 2
		};
		
		$.ajax({
			type: 'POST',
			url: '/WebAppProject/request',
			data: JSON.stringify(param),
			dataType: 'json',
			contentType: 'application/json',
			success: function(response){
		        console.log(response);
		        if(response.success){
		        	$("#notif").text(response.message);
		        	$('.accountCreditLimit input[type="text"]').val('');
		        }else{
		        	$("#notif").text(response.message);
		        }
			}
		});
	}
	</script>

	<b>&ensp;Change Credit Limit</b>
	<b><p id="notif"></p></b>
	<table id="accountCreditLimit" class="accountCreditLimit">
	<tr>
		<td>New Credit Limit : </td> 
		<td><input type="text" id="creditLimit" class="key-numeric" /></td>
	</tr>
	<tr>
		<td><input type="hidden" id="mobileNo" value="<%=session.getAttribute("mobileNo")%>" /></td>
		<td><input type="button" value="Submit" onClick="submitRequest()"/></td>
	</tr>
	</table>
</body>
</html>