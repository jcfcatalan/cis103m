<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.project.webapp.model.SubscriberReport" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/accordion.css">
</head>

<!-- <script type="text/javascript">
$(document).ready(function(e) {
	//$('#content').load('views/subscriber/dashboard.jsp');
	
	$.ajax({
		type: 'POST',
		url: '/WebAppProject/approver',
		data: JSON.stringify({mobileNo : "${sessionScope.mobileNo}"}),
		dataType: 'json',
		contentType: 'application/json',	//'application/json;charset=UTF-8', //'application/json',
		error: function(response){
			
		},
		success: function(response){
			console.log('Successfully submitted request.');
		}
	});
});
</script> -->

<script type="text/javascript">
<%
	session.setAttribute("subscriber", request.getAttribute("subscriber"));
%>

function deleteSubscriber(id){
	
}

function getRowValues(id, e){
	var rowIndex = e.parentNode.parentNode.rowIndex;
	var row = document.getElementById('tblSubscribers').rows[rowIndex];
	var arr = new Array();
	
	for(var j=0; j<row.cells.length; j++){
		arr.push(row.cells[j].innerHTML)
	}
	
	showUpdateSubscriber(id, arr);
}

function showUpdateSubscriber(id, arr){
	var tbl = document.getElementById('tblUpdateSubscriber');
	tbl.style.display='block';
	
	
	
	$('#subscriberId').val(id);
	$('#subscriberFName').val(arr[3]);
	$('#subscriberMName').val(arr[4]);
	$('#subscriberLName').val(arr[2]);
	$('#subscriberMobile').val(arr[5]);
	$('#subscriberTelephone').val();
	$('#subscriberEmail').val();
	$('#subscriberStreet').val();
	$('#subscriberCity').val();
	$('#subscriberProvince').val();
	$('#subscriberZipCode').val();
	
	console.log(tbl);
}

function updateSubscriber(id){
	
}

function cancelAction(tblName){
	document.getElementById(tblName).style.display='none';
}
</script>

<body>
	<input type="hidden" id="subscribers" value="<%=session.getAttribute("subscribers")%>" />
	<table id="tblSubscribers">
		<thead>
			<tr>
				<th>ID</th>
				<th>Account No</th>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Mobile No</th>
				<th>Billing Address</th>
				<th>Account Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${subscribers}" var="sub">
				<tr>
					<td>${sub.id}</td>
					<td>${sub.accountNo}</td>
					<td>${sub.lastName}</td>
					<td>${sub.firstName}</td>
					<td>${sub.middleName}</td>
					<td>${sub.mobileNo}</td>
					<td>${sub.street}</td>
					<td>${sub.accountStatus}</td>
					<td>&ensp;<input type="button" id="deleteBtn" value="Remove" onClick="deleteSubscriber(${sub.id})" /> 
					&ensp;<input type="button" id="updateBtn" value="Update" onClick="getRowValues(${sub.id}, this)" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<!-- UPDATE -->
	<br>
	<table id="tblUpdateSubscriber" style="display:none;">
		<tr>
			<td colspan=2>UPDATE DETAILS</td>
		</tr>
		<tr>
			<td colspan=2>error msg display here</td>
		</tr>
		<tr>
			<td colspan=2><input type="hidden" id="subscriberId" /></td>
		</tr>
		<tr>
			<td>First Name : </td>
			<td><input type="text" id="subscriberFName" placeholder="First Name" /></td>
		</tr>
		<tr>
			<td>Middle Name : </td>
			<td><input type="text" id="subscriberMName" placeholder="Middle Name" /></td>
		</tr>
		<tr>
			<td>Last Name : </td>
			<td><input type="text" id="subscriberLName" placeholder="Last Name" /></td>
		</tr>
		<tr>
			<td>Mobile Number : </td>
			<td><input type="text" id="subscriberMobile" placeholder="Mobile" /></td>
		</tr>
		<tr>
			<td>Telephone Number : </td>
			<td><input type="text" id="subscriberTelephone" placeholder="Telephone" /></td>
		</tr>
		<tr>
			<td>Email Address : </td>
			<td><input type="text" id="subscriberEmail" placeholder="Email" /></td>
		</tr>
		<tr>
			<td>Address 1 : </td>
			<td><input type="text" id="subscriberStreet" placeholder="House/Lot No., Street, Brgy" /></td>
		</tr>
		<tr>
			<td>Address 2 : </td>
			<td><input type="text" id="subscriberCity" placeholder="City / Town"/></td>
		</tr>
		<tr>
			<td>Province : </td>
			<td><input type="text" id="subscriberProvince" placeholder="Province" /></td>
		</tr>
		<tr>
			<td>Zip Code : </td>
			<td><input type="text" id="subscriberZipCode" placeholder="Zip Code" /></td>
		</tr>
		<tr>
			<td><input type="button" id="cancelBtn" value="Cancel" onClick="cancelAction('tblUpdateSubscriber')"</td>
			<td><input type="button" id="updateSubBtn" value="Update" onClick="updateSubscriber()" /></td>
		</tr>
	</table>
</body>
</html>