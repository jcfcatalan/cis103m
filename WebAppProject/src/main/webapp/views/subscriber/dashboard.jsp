<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Overview</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" >
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/accordion.css" >
</head>
<body>
	<div>
		<!-- Postpaid Plan -->
		<div class="postpaid" style="width:50%; margin: 10px; padding:5px; display:inline-block; float:left;">
			<table>
				<tr>
					<td>Plan</td>
					<td><%=session.getAttribute("planDescription")%></td>
				</tr>
				<tr>
					<td>Credit Limit</td>
					<td><%=session.getAttribute("planCreditLimit")%></td>
				</tr>
				<tr>
					<td>Contract Duration</td>
					<td><%=session.getAttribute("planContractDuration")%> Months</td>
				</tr>
				<tr>
					<td>Activation Date</td>
					<td><%=session.getAttribute("planActivationDate")%></td>
				</tr>
				<tr>
					<td>Contract End</td>
					<td><%=session.getAttribute("planContractEnd")%></td>
				</tr>
			</table>
		</div>
		
		<!-- Account Information -->
		<div class="account" style="width:50%; margin: 10px; padding:5px; display:inline-block; float:left;">
			<table>
				<tr>
					<td>Account Number</td>
					<td><%=session.getAttribute("accountNo")%></td>
				</tr>
				<tr>
					<td>Account Name</td>
					<td><%=session.getAttribute("subscriberName")%></td>
				</tr>
				<tr>
					<td>Mobile Number</td>
					<td><%=session.getAttribute("mobileNo")%></td>
				</tr>
				<tr>
					<td>Billing Address</td>
					<td><%=session.getAttribute("subscriberAddress")%></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>