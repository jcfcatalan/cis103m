<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Details</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<script>
$(document).ready(function() {
    $('.key-numeric').keypress(function(e) {
    	var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
        if (verified) {
        	e.preventDefault();
        }
    });
});

function showPayDiv(){
	$('#payment').toggle('show');
}

function payBills(){
	var param = {
		paidAmount	: $('#paidAmount').val(),
		mobileNo	: ${sessionScope.mobileNo},
		accountNo	: ${sessionScope.accountNo}
	};
	
	$.ajax({
		type: 'POST',
		url: '/WebAppProject/payment',
		data: JSON.stringify(param),
		dataType: 'json',
		contentType: 'application/json',
		success: function(response){
	        console.log(response);
	        if(response.success){
		        $("#amountDue").text(response.message);
		        $('.payment input[type="text"]').val('');
		    	$('#payment').toggle('show');
	        }else{
	        	$("#responseMessage").text(response.message);
	        }
		}
	});
}

function cancelAction(view){
	$('#payment').toggle('show');
    $('.payment input[type="text"]').val('');
}
</script>

<%
	String msg = (String) request.getAttribute("message");
	if (msg == null) {
		msg = "";
	}
%>
<body>
	<p><%=msg%></p>
	<div>
		<!-- Account Balance -->
		<div class="account" style="width:50%; margin: 10px; padding:5px; display:inline-block; float:left;">
		<b>&ensp;Balance</b>
			<table>
				<tr>
					<td>Amount Due</td>
					<td id="amountDue"><%=session.getAttribute("planBalance")%></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button" value="Pay Bills Now" onClick="showPayDiv()" /></td>
				</tr>
			</table>
		</div>
		
		<div class="payment" id="payment" style="width:50%; margin: 10px; padding:5px; float:left; display:none;">
			<p id="responseMessage"></p>
			<b>&ensp;Pay Bills</b>
			<table>
				<tr>
					<td>Enter Amount to Pay: </td>
					<td><input type="text" id="paidAmount" name="paidAmount" class="key-numeric" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button" value="Cancel" onClick="cancelAction('payment')" />
					&ensp;<input type="button" value="Pay" onClick="payBills()" /></td>
				</tr>
			</table>
		</div>
		
		<!-- Postpaid Plan -->
		<div class="postpaid" style="width:50%; margin: 10px; padding:5px; display:inline-block; float:left;">
		<b>&ensp;Postpaid Plan</b>
			<table>
				<tr>
					<td>Plan</td>
					<td><%=session.getAttribute("planDescription")%></td>
				</tr>
				<tr>
					<td>Credit Limit</td>
					<td><%=session.getAttribute("planCreditLimit")%></td>
				</tr>
				<tr>
					<td>Contract Duration</td>
					<td><%=session.getAttribute("planContractDuration")%> Months</td>
				</tr>
				<tr>
					<td>Activation Date</td>
					<td><%=session.getAttribute("planActivationDate")%></td>
				</tr>
				<tr>
					<td>Contract End</td>
					<td><%=session.getAttribute("planContractEnd")%></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>