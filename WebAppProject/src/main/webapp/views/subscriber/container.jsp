<%@page import="com.project.webapp.model.PostpaidPlan"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Subscriber</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" >
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/accordion.css" >
</head>

<script>
	$(document).ready(function(e) {
		$('#content').load('views/subscriber/dashboard.jsp');
		
		/* $.ajax({
			type: 'POST',
			url: '/WebAppProject/account',
			data: JSON.stringify({mobileNo : "${sessionScope.mobileNo}"}),
			dataType: 'json',
			contentType: 'application/json',	//'application/json;charset=UTF-8', //'application/json',
			error: function(response){
				
			},
			success: function(response){
				console.log('Successfully submitted request.');
			}
		}); */
	});
</script>

<script type="text/javascript">
function viewAccountOverview(){
	$('#content').load('views/subscriber/overview.jsp');
}

function viewProfile(){
	$('#content').load('views/subscriber/profile.jsp');
}

function changeBilling(){
	$('#content').load('views/subscriber/changeBilling.jsp');
}

function changeCreditLimit(){
	$('#content').load('views/subscriber/changeCreditLimit.jsp');
}

function logOut(){
	console.log('must log out');
	$.ajax({
		type: 'POST',
		url: '/WebAppProject/logout',
		data: JSON.stringify({mobileNo : "${sessionScope.mobileNo}"}),
		dataType: 'json',
		contentType: 'application/json',
		error: function(response){
			
		},
		success: function(response){
			console.log('Successfully submitted request.');
		}
	});
}
</script>

<body>

<div class="flex-container">
	<header></header>
	
	<div class="nav">
		<button class="accordion">Account</button>
		<div class="panel">
			<button id="accountOverview" class="acc-sib" onClick="viewAccountOverview()">Overview</button>
			<button id="subscriberProfile" class="acc-sib" onClick="viewProfile()">Profile</button>
		</div>
		<button class="accordion">Request</button>
		<div class="panel">
			<button id="" class="acc-sib" onClick="changeBilling()">Change Billing Address</button>
			<button id="" class="acc-sib" onClick="changeCreditLimit()">Change Credit Limit</button>
		</div>
		<button class="accordion-last" onClick="logOut()">Log Out</button>
		
		<script>
			var accordion = document.getElementsByClassName("accordion");
			for(var i=0; i<accordion.length; i++){
				accordion[i].onclick = function(){
					this.classList.toggle("active");
					this.nextElementSibling.classList.toggle("show");
				}
			}
		</script>
	</div>
	
	<div class="content" id="content">
		<%
			PostpaidPlan plan = (PostpaidPlan) request.getAttribute("planDetails");
			
    		session.setAttribute("mobileNo", request.getAttribute("mobileNo"));
    		session.setAttribute("accountNo", request.getAttribute("accountNo"));
    		session.setAttribute("subscriberId", request.getAttribute("subscriberId"));
    		session.setAttribute("subscriberName", request.getAttribute("subscriberName"));
    		session.setAttribute("subscriberAddress", request.getAttribute("subscriberAddress"));
    		session.setAttribute("subscriberEmail", request.getAttribute("subscriberEmail"));
    		
    		session.setAttribute("planId", request.getAttribute("planId"));
    		session.setAttribute("planDescription", request.getAttribute("planDescription"));
    		session.setAttribute("planCreditLimit", request.getAttribute("planCreditLimit"));
    		session.setAttribute("planContractDuration", request.getAttribute("planContractDuration"));
    		session.setAttribute("planActivationDate", request.getAttribute("planActivationDate"));
    		session.setAttribute("planContractEnd", request.getAttribute("planContractEnd"));
    		session.setAttribute("planBalance", request.getAttribute("planBalance"));
        %>
	</div>
	
	<footer>&copy; 2016</footer>
</div>

</body>
</html>