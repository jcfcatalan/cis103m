<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<style type="text/css">
* {
    box-sizing: border-box;
}

body {
	font: 12px arial, san-serif;
}

.flex-container {
	display: -webkit-flex;
	display: flex;
	-webkit-fles-flow: row wrap;
	flex-flow: row wrap;
	text-align: center;
	height: 100%;
}

.flex-container > * {
	padding: 10px;
	-webkit-flex: 1 100%;
	flex: 1 100%;
}

.nav {
	max-width: 20%;
}

.nav ul {
	list-style-type: none;
	padding: 0;
}

.nav ul a {
	text-decoration: none;
}

.content {
	text-align: left;
	padding: 60px;
	margin: 60px
}

header {
	background: #333;
	color: white;
	max-height: 20%;
}

footer {
	background: #aaa;
	color: white;
	max-height: 20%;
	text-align: right;
}

@media all and (min-width: 768px) {
	.nav {
		text-align: left;
		-webkit-flex: 1 auto;
		flex: 1 auto;
		-webkit-order: 1;
		order: 1;
	}
	.content {
		-webkit-flex: 5 0px;
		flex: 5 0px;
		-webkit-order: 2;
		order: 2;
	}
	footer {
		-webkit-order: 3;
		order: 3;
	}
}

p {
	color: green;
	font-weight: bold;
}
</style>
</head>

<%
	String msg = (String) request.getAttribute("message");
	if(msg == null){
		msg = "";
	}
%>
<body>
	<div class="flex-container">
		<header></header>
		
		<div class="nav"></div>
		
		<div class="content">
			<br>
			<center><p><%= msg %></p></center>
			<br>
		</div>
		
		<footer>&copy; 2016</footer>
	</div>
</body>
</html>