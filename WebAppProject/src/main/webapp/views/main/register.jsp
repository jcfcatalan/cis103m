<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>

<style type="text/css">
* {
    box-sizing: border-box;
}

body {
	font: 12px arial, san-serif;
}

.flex-container {
	display: -webkit-flex;
	display: flex;
	-webkit-fles-flow: row wrap;
	flex-flow: row wrap;
	text-align: center;
	height: 100%;
}

.flex-container > * {
	padding: 10px;
	-webkit-flex: 1 100%;
	flex: 1 100%;
}

.nav {
	max-width: 20%;
}

.nav ul {
	list-style-type: none;
	padding: 0;
}

.nav ul a {
	text-decoration: none;
}

.content {
	text-align: left;
	padding: 60px;
	margin: 60px
}

header {
	background: #333;
	color: white;
	max-height: 20%;
}

footer {
	background: #aaa;
	color: white;
	max-height: 20%;
	text-align: right;
}

@media all and (min-width: 768px) {
	.nav {
		text-align: left;
		-webkit-flex: 1 auto;
		flex: 1 auto;
		-webkit-order: 1;
		order: 1;
	}
	.content {
		-webkit-flex: 5 0px;
		flex: 5 0px;
		-webkit-order: 2;
		order: 2;
	}
	footer {
		-webkit-order: 3;
		order: 3;
	}
}

p {
	color: red;
	font-weight: bold;
}
</style>
</head>

<script>
$(document).ready(function() {
    $('.key-numeric').keypress(function(e) {
    	var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
        if (verified) {
        	e.preventDefault();
        }
    });
});

function cancelAction(view){
    parent.history.back();
	return false;
}
</script>

<%
	String msg = (String) request.getAttribute("message");
	if (msg == null) {
		msg = "";
	}
%>
<body>
<div class="flex-container">
		<header></header>
		
		<div class="nav"></div>
		
		<div class="content">
			<p><%=msg%></p>
			<br>
			<form method="post" action="create">
				<table id="subscriberDetails" class="subscriberDetails">
					<tr>
						<td>Mobile Number :</td>
						<td><input type="text" id="mobileNo" name="mobileNo" /></td>
					</tr>
					<tr>
						<td>First Name :</td>
						<td><input type="text" id="firstName" name="firstName" /></td>
					</tr>
					<tr>
						<td>Last Name :</td>
						<td><input type="text" id="lastName" name="lastName" /></td>
					</tr>
					<tr>
						<td>Password :</td>
						<td><input type="password" id="password" name="password" /></td>
					</tr>
					<tr>
						<td>Confirm Password :</td>
						<td><input type="password" id="confirmPassword" name="confirmPassword" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="button" value="Cancel" onClick="cancelAction('subscriberDetails')" />
						&ensp;<input type="submit" value="Register" /></td>
					</tr>
				</table>
			</form>
		</div>
		
		<footer>&copy; 2016</footer>
	</div>
</body>
</html>