package com.project.webapp.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.util.Validator;

public class LoginController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3526256252569500629L;
	
	private static final String USER = "username";
	private static final String PASSWORD = "password";
	private static final String MESSAGE = "message";
	private static final String LOGIN_VIEW = "/views/main/login.jsp";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//initializing validator class
		Validator validator = new Validator();
        
		//reading the user input
        String username = request.getParameter(USER);
        String password = request.getParameter(PASSWORD);
        
        /*if(validator.isNumeric(username)){
        	subscriberLogin(username, password, request, response);
        }else{
        	approverLogin(username, password, request, response);
        }*/
        
        if(validator.checkUser(username, password)){
        	if(validator.isNumeric(username)){
        		request.setAttribute("mobileNo", username);
            	request.getRequestDispatcher("/welcome").forward(request, response);
            }else{
        		request.setAttribute("username", username);
            	request.getRequestDispatcher("/approver").forward(request, response);
            }
        }else{
        	request.setAttribute(MESSAGE, "Username or Password is incorrect.");
            request.getRequestDispatcher(LOGIN_VIEW).forward(request, response);
        }
	}
	
	@SuppressWarnings("unused")
	private void subscriberLogin(String mobileNo, String password, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Validator validator = new Validator();		
		if(validator.checkSubscriber(mobileNo, password)) {
        	request.setAttribute("mobileNo", mobileNo);
        	request.getRequestDispatcher("/welcome").forward(request, response);
//            RequestDispatcher rd = request.getRequestDispatcher("/welcome");
//            rd.forward(request, response);
        } else {
           request.setAttribute(MESSAGE, "Username or Password is incorrect");
           request.getRequestDispatcher(LOGIN_VIEW).forward(request, response);
//           RequestDispatcher rd = request.getRequestDispatcher("/login");
//           rd.include(request, response);
        }
	}
	
	@SuppressWarnings("unused")
	private void approverLogin(String username, String password, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Validator validator = new Validator();
		if(validator.checkApprover(username, password)) {
        	request.setAttribute("username", username);
        	request.getRequestDispatcher("/approver").forward(request, response);
        } else {
           request.setAttribute(MESSAGE, "Username or Password is incorrect");
           request.getRequestDispatcher(LOGIN_VIEW).forward(request, response);
        }
	}
}