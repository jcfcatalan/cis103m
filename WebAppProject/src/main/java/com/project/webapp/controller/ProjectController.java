package com.project.webapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.SubscriberDao;
import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;

public class ProjectController {

	private SubscriberDao subscriberDao;
	
	public List<Subscriber> getAllSubscriber(){
		List<Subscriber> subscribers = new ArrayList<Subscriber>();
		subscribers = subscriberDao.getAllSubscriber();
		return subscribers;
	}
	
	public void addSubscriber(HttpServletRequest request, HttpServletResponse response){
		Subscriber subscriber = new Subscriber();
		subscriber.setFirstName("");
		subscriber.setMiddleName("");
		subscriber.setLastName("");
		subscriber.setStreet("");
		subscriber.setCity("");
		subscriber.setProvince("");
		subscriber.setZipCode(123);
		subscriber.setEmailAddress("");
		
		subscriberDao.addSubscriber(subscriber);
	}
	
	public void updateSubscriberDetails(HttpServletRequest request, HttpServletResponse response){
		Subscriber subscriber = new Subscriber();
		subscriber.setFirstName("");
		subscriber.setMiddleName("");
		subscriber.setLastName("");
		subscriber.setStreet("");
		subscriber.setCity("");
		subscriber.setProvince("");
		subscriber.setZipCode(123);
		subscriber.setEmailAddress("");
		
		ServiceRequest serviceRequest = new ServiceRequest();
		serviceRequest.setAccountNo(123);
		serviceRequest.setSubscriberId(123);
		serviceRequest.setDescription("");
		serviceRequest.setRequestedDate("");
		serviceRequest.setRequestStatus("PENDING");
		
		subscriberDao.requestChangeBillingAddress(subscriber, serviceRequest);		
	}
}