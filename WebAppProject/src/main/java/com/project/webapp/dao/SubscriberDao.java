package com.project.webapp.dao;

import java.util.List;

import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;

public interface SubscriberDao {
	
	public List<Subscriber> getAllSubscriber();
	
	public void addSubscriber(Subscriber subscriber);
	
	public PostpaidAccount getAccountDetails();
	
	public void requestChangeBillingAddress(Subscriber subscriber, ServiceRequest serviceRequest);
	
	public void requestChangeCreditLimit();
}