package com.project.webapp.dao.impl.jc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.PostpaidPlan;

public class PostpaidPlanDaoImpl implements PostpaidPlanDao{
	SQLDao sqlDao =  new SQLDao();

	@Override
	public void setupReadPst(PreparedStatement pst, long id) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PostpaidPlan build(ResultSet rst) throws SQLException {
		PostpaidPlan postpaidPlan =  new PostpaidPlan();
		postpaidPlan.setId(rst.getInt("id"));
		postpaidPlan.setContractPeriod(rst.getInt("contractPeriod"));
		postpaidPlan.setMsf(rst.getInt("msf"));
		postpaidPlan.setDescription(rst.getString("description"));
		return postpaidPlan;
	}

	@Override
	public void setupUpdatePst(PreparedStatement pst, PostpaidPlan data) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setupInsertPst(PreparedStatement pst, PostpaidPlan data) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<PostpaidPlan> getAllPostpaidPlans() {
		List<PostpaidPlan> postpaidPlans = new ArrayList<PostpaidPlan>();
		ResultSet resultSet = null;
		String sql = "SELECT * from postpaid_plan";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				postpaidPlans.add(build(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			sqlDao.closeConnection();
		}
		return postpaidPlans;
	}
	

}
