package com.project.webapp.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.project.webapp.dao.ApproverDao;
import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.ApproverReport;
import com.project.webapp.model.Subscriber;

public class ApproverDaoImpl implements ApproverDao {

	SQLDao sqlDao = new SQLDao();

	@Override
	public void addSubscriber(Subscriber subscriber) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Subscriber> getAllSubscriber() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void approveChangeBillingRequest() {
		// TODO Auto-generated method stub

	}

	@Override
	public void approveChangeCreditLimitRequest() {
		// TODO Auto-generated method stub

	}

	public ApproverReport buildAR(ResultSet rs) throws SQLException {
		ApproverReport ar = new ApproverReport();
		ar.setApprovalCount(rs.getInt("count"));
		ar.setLastName(rs.getString("lastName"));
		ar.setFirstName(rs.getString("firstName"));
		ar.setMiddleName(rs.getString("middleName"));
		ar.setId(rs.getInt("id"));
		return ar;
	}
	
	public ApproverReport buildAR2(ResultSet rs) throws SQLException {
		ApproverReport ar = new ApproverReport();
		ar.setLastName(rs.getString("lastName"));
		ar.setFirstName(rs.getString("firstName"));
		ar.setMiddleName(rs.getString("middleName"));
		ar.setAccountNo(rs.getInt("accountNo"));
		ar.setModifiedDate(rs.getString("modificationDate"));
		ar.setRequestedDate(rs.getString("requestedDate"));
		String approverName = rs.getString("afName") + " " + rs.getString("amName") + " "
				+ rs.getString("alName");
		ar.setApprover(approverName);
		ar.setDescription(rs.getString("description"));
		return ar;
	}

	@Override
	public List<ApproverReport> approvalReport() {
		List<ApproverReport> srl = new ArrayList<ApproverReport>();
		ResultSet resultSet = null;
		String sql = "SELECT ap.id , ap.lastName, ap.firstName, ap.middleName, count(pa.approverId) as count from approver ap, postpaid_account pa where ap.id = pa.approverId group by ap.id";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				srl.add(buildAR(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return srl;
	}

	@Override
	public List<ApproverReport> requestApprovalReport() {
		List<ApproverReport> srl = new ArrayList<ApproverReport>();
		ResultSet resultSet = null;
		String sql = "SELECT sr.description, sr.subscriberId, sr.approverId, s.firstName, s.lastName, s.middleName, sr.accountNo, sr.modificationDate, sr.requestedDate, ap.firstName as afName, ap.lastName as alName, ap.middleName as amName FROM approver ap, subscriber s, service_request sr where ap.id =sr.approverId and sr.subscriberId = s.id and sr.requestStatus = 'A' ORDER BY s.lastName";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				srl.add(buildAR2(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return srl;
	}
}