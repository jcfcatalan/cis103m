package com.project.webapp.dao.impl.jc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.ContactDetail;
import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;
import com.project.webapp.model.SubscriberReport;

public class SubscriberDaoImpl implements SubscriberDao {
	SQLDao sqlDao = new SQLDao();

	@Override
	public void setupReadPst(PreparedStatement pst, long id) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Subscriber build(ResultSet rst) throws SQLException {
		Subscriber subscriber = new Subscriber();
		subscriber.setId(rst.getInt("id"));
		subscriber.setFirstName(rst.getString("firstName"));
		subscriber.setMiddleName(rst.getString("middleName"));
		subscriber.setLastName(rst.getString("lastName"));
		subscriber.setEmailAddress(rst.getString("emailAddress"));
		subscriber.setStreet(rst.getString("street"));
		subscriber.setCity(rst.getString("province"));
		subscriber.setZipCode(rst.getInt("zipCode"));
		return subscriber;
	}

	public SubscriberReport buildSR(ResultSet rs) throws SQLException {
		SubscriberReport sr = new SubscriberReport();
		sr.setId(rs.getInt("subscriberId"));
		sr.setFirstName(rs.getString("sFirstName"));
		sr.setMiddleName(rs.getString("sMiddleName"));
		sr.setLastName(rs.getString("sLastName"));
		sr.setMobileNo(rs.getString("postpaidNo"));
		sr.setEmailAddress(rs.getString("emailAddress"));
		sr.setStreet(rs.getString("street"));
		sr.setProvince(rs.getString("province"));
		sr.setCity(rs.getString("city"));
		sr.setZipCode(rs.getInt("zipCode"));
		sr.setApproverId(rs.getInt("approverId"));
		sr.setAccountNo(rs.getLong("accountNo"));
		sr.setPlanId(rs.getInt("planId"));
		sr.setPlanDescription(rs.getString("description"));
		sr.setZipCode(rs.getInt("zipCode"));
		sr.setActivationDate(rs.getString("activationDate"));
		sr.setAccountStatus(rs.getString("accountStatus"));
		String approverName = rs.getString("aFirstName") + " " + rs.getString("aMiddleName") + " "
				+ rs.getString("aLastName");
		sr.setApprover(approverName);
		sr.setCreditLimit(rs.getDouble("creditLimit"));
		return sr;
	}

	public SubscriberReport buildSR2(ResultSet rs) throws SQLException {
		SubscriberReport sr = new SubscriberReport();
		sr.setId(rs.getInt("subscriberId"));
		sr.setFirstName(rs.getString("firstName"));
		sr.setMiddleName(rs.getString("middleName"));
		sr.setLastName(rs.getString("lastName"));
		sr.setMobileNo(rs.getString("postpaidNo"));
		sr.setEmailAddress(rs.getString("emailAddress"));
		sr.setStreet(rs.getString("street"));
		sr.setProvince(rs.getString("province"));
		sr.setCity(rs.getString("city"));
		sr.setZipCode(rs.getInt("zipCode"));
		sr.setAccountNo(rs.getLong("accountNo"));
		sr.setPlanId(rs.getInt("planId"));
		sr.setPlanDescription(rs.getString("description"));
		sr.setZipCode(rs.getInt("zipCode"));
		sr.setActivationDate(rs.getString("activationDate"));
		sr.setAccountStatus(rs.getString("accountStatus"));
		sr.setCreditLimit(rs.getDouble("creditLimit"));
		sr.setMobileOtherNo(rs.getString("mobileNo"));
		sr.setTelephoneNo(rs.getString("telephoneNo"));
		return sr;
	}

	@Override
	public void setupInsertPst(PreparedStatement pst, Subscriber data) throws SQLException {
		pst.clearParameters();
		int pos = 0;
		pst.setString(++pos, data.getFirstName());
		pst.setString(++pos, data.getMiddleName());
		pst.setString(++pos, data.getLastName());
		pst.setString(++pos, data.getEmailAddress());
		pst.setString(++pos, data.getCity());
		pst.setString(++pos, data.getStreet());
		pst.setString(++pos, data.getProvince());
		pst.setInt(++pos, data.getZipCode());
		pst.setString(++pos, data.getMobileNo());
		pst.setString(++pos, data.getTelephoneNo());
		pst.setString(++pos, data.getOfficeNo());

	}

	@Override
	public void setupUpdatePst(PreparedStatement pst, Subscriber data) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<SubscriberReport> getSubscriberReport() {
		List<SubscriberReport> sr = new ArrayList<SubscriberReport>();
		ResultSet resultSet = null;
		String sql = "SELECT pa.subscriberId, pa.planId, pa.approverId, s.firstName as sFirstName, pa.postpaidNo,"
				+ "s.middleName as sMiddleName, s.lastName as sLastName, s.emailAddress, s.street, s.city,"
				+ "s.province, pa.activationDate, pa.accountStatus, pa.accountNo,s.zipCode, s.mobileNo, s.zipCode, "
				+ "s.telephoneNo, s.officeNo, ap.firstName as aFirstName, ap.middleName as aMiddleName,  "
				+ "ap.lastName as aLastName, pa.creditLimit, pp.description, pp.msf, pp.contractPeriod "
				+ "FROM subscriber s, approver ap, postpaid_account pa, postpaid_plan pp WHERE "
				+ "s.id = pa.subscriberId and ap.id = pa.approverId and pa.planId = pp.id  and pa.accountStatus = 'A' ORDER BY pa.accountNo";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				sr.add(buildSR(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return sr;
	}

	@Override
	public List<Subscriber> getAll() {
		List<Subscriber> subscribers = new ArrayList<Subscriber>();
		ResultSet resultSet = null;
		String sql = "SELECT * from subscriber";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				subscribers.add(build(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return subscribers;
	}

	@Override
	public boolean add(Subscriber subscriber, int planId) {
		boolean success = false;
		PreparedStatement ps = null;
		String sql = "INSERT INTO subscriber (firstName, middleName, lastName, "
				+ "emailAddress, street, city, province, zipCode, mobileNo, telephoneNo, officeNo) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		try {
			Connection connection = sqlDao.establishConnection();
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			setupInsertPst(ps, subscriber);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			int subscriberId = rs.getInt(1);
			// for (ContactDetail contactDetail :
			// subscriber.getContactDetails()) {
			// contactDetail.setSubscriberId(subscriberId);
			// addSubscriberDetails(contactDetail);
			// }
			addPostpaidAccount(subscriberId, planId);
			success = true;
		} catch (SQLException e) {
			success = false;
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}

		return success;
	}

	private void addSubscriberDetails(ContactDetail contactDetail) throws SQLException {
		PreparedStatement ps = null;
		String sql = "INSERT INTO subscriber_contactdetails (subscriberId, contactNo, type) " + "VALUES (?, ?, ?);";
		try {
			Connection connection = sqlDao.establishConnection();
			ps = connection.prepareStatement(sql);

			ps.setInt(1, contactDetail.getSubscriberId());
			ps.setString(2, contactDetail.getContactNo());
			ps.setString(3, contactDetail.getType());
			ps.executeUpdate();
		} finally {
			sqlDao.closeConnection();
		}

	}

	private void addPostpaidAccount(int subscriberId, int planId) throws SQLException {
		PreparedStatement ps = null;
		String sql = "INSERT INTO postpaid_account (subscriberId, accountStatus, planId) " + "VALUES (?, ?, ?);";
		try {
			Connection connection = sqlDao.establishConnection();
			ps = connection.prepareStatement(sql);

			ps.setInt(1, subscriberId);
			ps.setString(2, "P");
			ps.setInt(3, planId);
			ps.executeUpdate();
		} finally {
			sqlDao.closeConnection();
		}

	}

	@Override
	public void update(Subscriber subscriber, ServiceRequest serviceRequest) {
		// TODO Auto-generated method stub

	}

	@Override
	public Subscriber read(long subcriberId) {
		Subscriber subscriber = null;
		ResultSet resultSet = null;
		String sql = "SELECT * from subscriber";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				subscriber = build(resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return subscriber;
	}

	@Override
	public List<SubscriberReport> getAllApplications() {
		List<SubscriberReport> srl = new ArrayList<SubscriberReport>();
		ResultSet resultSet = null;
		String sql = "SELECT * FROM subscriber s, postpaid_account pa, postpaid_plan pp where s.id = pa.subscriberId and pp.id = pa.planId and pa.accountStatus = 'P'";
		try {
			Connection connection = sqlDao.establishConnection();
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				srl.add(buildSR2(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		return srl;
	}
}