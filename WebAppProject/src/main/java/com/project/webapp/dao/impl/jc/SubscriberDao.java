package com.project.webapp.dao.impl.jc;

import java.util.List;

import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;
import com.project.webapp.model.SubscriberReport;

public interface SubscriberDao extends Dao<Subscriber>{

	public Subscriber read(long subscriberId);
	
	public void update(Subscriber subscriber, ServiceRequest serviceRequest);

	List<Subscriber> getAll(); 
	
	boolean add(Subscriber subscriber, int planId);
	
	List<SubscriberReport> getAllApplications(); 

	List<SubscriberReport> getSubscriberReport();
}