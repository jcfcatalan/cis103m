package com.project.webapp.dao;

import java.util.List;

import com.project.webapp.model.ApproverReport;
import com.project.webapp.model.Subscriber;

public interface ApproverDao {

	public void addSubscriber(Subscriber subscriber);
	
	public List<Subscriber> getAllSubscriber();
	
	public void approveChangeBillingRequest();
	
	public void approveChangeCreditLimitRequest();
	
	public List<ApproverReport> approvalReport();
	
	public List<ApproverReport> requestApprovalReport();
 }