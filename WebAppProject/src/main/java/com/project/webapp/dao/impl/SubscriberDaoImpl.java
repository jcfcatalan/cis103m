package com.project.webapp.dao.impl;

import java.sql.ResultSet;
import java.util.List;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.dao.SubscriberDao;
import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;

public class SubscriberDaoImpl implements SubscriberDao {
	
	private SQLDao sqlDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Subscriber> getAllSubscriber() {
		
		ResultSet result = sqlDao.selectAllFromTable("SUBSCRIBER");
				
		return (List<Subscriber>) result;
	}

	@Override
	public void addSubscriber(Subscriber subscriber) {
		
		sqlDao.insertSubscriberDetails(subscriber);
		
//		Connection connection = null;
//		connection = sqlDao.establishConnection();
//		
//		if(connection != null){
//			System.out.println("Connected!");
//			try {
//				String query = sqlDao.selectAllFromTable("SUBSCRIBER");
//				Statement statement = connection.createStatement();
//				statement.executeUpdate(query);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
	}

	@Override
	public PostpaidAccount getAccountDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void requestChangeBillingAddress(Subscriber subscriber, ServiceRequest serviceRequest) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestChangeCreditLimit() {
		// TODO Auto-generated method stub
		
	}
}