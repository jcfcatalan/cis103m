package com.project.webapp.dao.impl.jc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public interface Dao<T> {

    public void setupReadPst(PreparedStatement pst, long id) throws SQLException;

    public T build(ResultSet rst) throws SQLException;

    public void setupUpdatePst(PreparedStatement pst, T data) throws SQLException;

    public void setupInsertPst(PreparedStatement pst, T data) throws SQLException;
}
