package com.project.webapp.dao.impl.jc;

import java.util.List;

import com.project.webapp.model.PostpaidPlan;

public interface PostpaidPlanDao extends Dao<PostpaidPlan>{
	
	List<PostpaidPlan> getAllPostpaidPlans();

}
