package com.project.webapp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.project.webapp.model.Approver;
import com.project.webapp.model.ServiceRequest;
import com.project.webapp.model.Subscriber;


public class SQLDao {

	private static final String jdbcDriver = "com.mysql.jdbc.Driver";
	private static final String sqlDb = "jdbc:mysql://192.168.0.13:3306/cis103m";
	private static final String sqlUser = "test";
	private static final String sqlPassword = "test";
	
	private static final String SELECT_ALL = "SELECT * FROM ";
	private static final String INSERT_INTO_SUBSCRIBER = "INSERT INTO SUBSCRIBER ";
	private static final String INSERT_INTO_APPROVER = "INSERT INTO APPROVER ";
	private static final String UPDATE_SUBSCRIBER = "UPDATE SUBSCRIBER SET ";
	private static final String UPDATE_POSTPAID_ACCOUNT = "UPDATE POSTPAID_ACCOUNT SET ";	
//	private static final String DELETE = "DELETE FROM ";

	Connection connection = null;

	public Connection establishConnection(){
		//loading drivers for mysql
		try{
			Class.forName(jdbcDriver);			
		}catch(ClassNotFoundException exception){
			System.out.println("MySQL Driver not found.");
			exception.printStackTrace();
		}
		
		//connecting to database
		try{
			connection = DriverManager.getConnection(sqlDb, sqlUser, sqlPassword);
		}catch(SQLException exception){
			System.out.println("MySQL connection failed.");
			exception.printStackTrace();
		}
		
		if(connection != null){
			System.out.println("Connected to Database!");
		}
		
		return connection;
	}
	
	public void closeConnection(){
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Select all in a given table
	 * 
	 * @param tblName
	 * @return resultSet
	 */
	public ResultSet selectAllFromTable(String tblName){
		String sql = SELECT_ALL + tblName;

		ResultSet resultSet = null;
		Connection connection = null;
		connection = establishConnection();
		
		if(connection != null){
			System.out.println("Connected!");
			try {
				Statement statement = connection.createStatement();
				resultSet = statement.executeQuery(sql);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return resultSet;
	}
	
	public boolean insertSubscriberDetails(Subscriber subscriber){
		boolean success = false;
		String sql = INSERT_INTO_SUBSCRIBER + "(firstName, middleName, lastName, emailAddress, street, city, province, zipCode)";
		
		StringBuilder sb = new StringBuilder(sql);
		sb.append(" VALUES (");
		sb.append(subscriber.getFirstName() + ", ");
		sb.append(subscriber.getMiddleName() + ", ");
		sb.append(subscriber.getLastName() + ", ");
		sb.append(subscriber.getEmailAddress() + ", ");
		sb.append(subscriber.getStreet() + ", ");
		sb.append(subscriber.getCity() + ", ");
		sb.append(subscriber.getProvince() + ", ");
		sb.append(subscriber.getZipCode() + ")");
		
		Connection connection = null;
		connection = establishConnection();
		
		try {
			if(connection != null){
				Statement statement = connection.createStatement();
				int rowCount = statement.executeUpdate(sql);
				
				if(rowCount == 1){
					success = true;
				}
				System.out.println("No. of Inserted Row : " + rowCount);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		
		return success;
	}
	
	public boolean insertApproverDetails(Approver approver){
		boolean success = false;
		String sql = INSERT_INTO_APPROVER + "(firstName, middleName, lastName, emailAddress)";
		
		StringBuilder sb = new StringBuilder(sql);
		sb.append(" VALUES (");
		sb.append(approver.getFirstName() + ", ");
		sb.append(approver.getMiddleName() + ", ");
		sb.append(approver.getLastName() + ", ");
		sb.append(approver.getEmailAddress() + ", ");
		
		Connection connection = null;
		connection = establishConnection();
		
		try {
			if(connection != null){
				Statement statement = connection.createStatement();
				int rowCount = statement.executeUpdate(sql);
				
				if(rowCount == 1){
					success = true;
				}
				System.out.println("No. of Inserted Row : " + rowCount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	public boolean updateSubcriberDetails(Subscriber subscriber, ServiceRequest serviceRequest){
		boolean success = false;
		StringBuilder sb = new StringBuilder(UPDATE_SUBSCRIBER);
		sb.append("NEWSTREET = " + subscriber.getStreet() + ", ");
		sb.append("NEWCITY = " + subscriber.getCity() + ", ");
		sb.append("NEWPROVINCE = " + subscriber.getProvince() + ", ");
		sb.append("NEWZIPCODE = " + subscriber.getZipCode());
		
		Connection connection = null;
		connection = establishConnection();
		
		try {
			if(connection != null){
				Statement statement = connection.createStatement();
				int rowCount = statement.executeUpdate(sb.toString());
				
				if(rowCount == 1){
					success = true;
				}
				System.out.println("No. of Inserted Row : " + rowCount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	public boolean updatePostpaidAccount(ServiceRequest serviceRequest){
		boolean success = false;
		StringBuilder sb = new StringBuilder(UPDATE_POSTPAID_ACCOUNT);
		//sb.append("NEWCREDITLIMIT = " + subscriber.getStreet());
		
		Connection connection = null;
		connection = establishConnection();
		
		try {
			if(connection != null){
				Statement statement = connection.createStatement();
				int rowCount = statement.executeUpdate(sb.toString());
				
				if(rowCount == 1){
					success = true;
				}
				System.out.println("No. of Inserted Row : " + rowCount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
	}
}