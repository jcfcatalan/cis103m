package com.project.webapp.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.webapp.model.MessageResponse;

public class ResponseFormatter {

	public HttpServletResponse constructResponse(HttpServletResponse response, String message, boolean success) throws JsonProcessingException, IOException{
		
		JSONFormatter jsonFormatter = new JSONFormatter();

		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonFormatter.toJsonString(new MessageResponse(message, success)));
	    
	    return response;
	}
}