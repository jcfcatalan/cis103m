package com.project.webapp.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.PostpaidAccount;

public class Validator {

	private SQLDao sqlDao;
	
	private static final String sqlQuery_checkSubscriber = "SELECT * FROM POSTPAID_ACCOUNT WHERE POSTPAIDNO=? and PASSWORD=?";
	private static final String sqlQuery_checkApprover = "SELECT * FROM APPROVER WHERE EMAILADDRESS=? and PASSWORD=?";
	private static final String sqlQuery_checkNumber = "SELECT * FROM POSTPAID_ACCOUNT WHERE POSTPAIDNO=?";
	private static final String sqlQuery_checkUser = "SELECT USERNAME, PASSWORD FROM LOGIN WHERE USERNAME=? AND PASSWORD=?;";
	
	public boolean checkUser(String postpaidNo, String password) {
		boolean ifExist = false;
		try {
			//initializing dao class
			sqlDao = new SQLDao();
			
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_checkUser);
			ps.setString(1, postpaidNo);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			ifExist = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return ifExist;
	}	
	
	public boolean checkSubscriber(String postpaidNo, String password) {
		boolean ifExist = false;
		try {
			//initializing dao class
			sqlDao = new SQLDao();
			
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_checkSubscriber);
			ps.setString(1, postpaidNo);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			ifExist = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return ifExist;
	}
	
	public boolean checkApprover(String username, String password) {
		boolean ifExist = false;
		try {
			//initializing dao class
			sqlDao = new SQLDao();
			
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_checkApprover);
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			ifExist = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return ifExist;
	}
	
	public PostpaidAccount checkPostpaidAccount(String postpaidNo){
		PostpaidAccount account = null;
		try {
			sqlDao = new SQLDao();
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_checkNumber);
			ps.setString(1, postpaidNo);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				account = new PostpaidAccount();
				account.setAccountNo(rs.getLong("accountNo"));
				account.setPostpaidNo(rs.getLong("postpaidNo"));
				account.setPlanId(rs.getLong("planId"));
				account.setSubscriberId(rs.getLong("subscriberId"));
				account.setActivationDate(rs.getString("activationDate"));
				account.setCreditLimit(rs.getDouble("creditLimit"));
				account.setBalance(rs.getDouble("balance"));
				account.setAccountStatus(rs.getString("accountStatus"));
				account.setApproverId(rs.getLong("approverId"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return account;
	}
	
	public boolean checkPostpaidNo(String postpaidNo){
		boolean ifExist = false;
		try {
			sqlDao = new SQLDao();
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_checkNumber + " AND PASSWORD=NULL");
			ps.setString(1, postpaidNo);
			
			ResultSet rs = ps.executeQuery();
			ifExist = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return ifExist;
	}
	
	public boolean isNumeric(String s) {  
	    return s.matches("[-+]?\\d*\\.?\\d+");  
	}
}