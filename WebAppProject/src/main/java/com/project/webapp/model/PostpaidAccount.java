/**
 * 
 */
package com.project.webapp.model;

/**
 * @author loire
 *
 */
public class PostpaidAccount {

	private long accountNo;
	private long postpaidNo;
	private long planId;
	private long subscriberId;
	private String activationDate;
	private double creditLimit;
	private double balance;
	private String status;
	private long approverId;
	private String password;
	
	public long getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}
	
	public long getPostpaidNo() {
		return postpaidNo;
	}
	public void setPostpaidNo(long postpaidNo) {
		this.postpaidNo = postpaidNo;
	}
	
	public long getPlanId() {
		return planId;
	}
	public void setPlanId(long planId) {
		this.planId = planId;
	}
	
	public long getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(long subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	
	public double getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public String getAccountStatus() {
		return status;
	}
	public void setAccountStatus(String accountStatus) {
		this.status = accountStatus;
	}
	
	public long getApproverId() {
		return approverId;
	}
	public void setApproverId(long approverId) {
		this.approverId = approverId;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}