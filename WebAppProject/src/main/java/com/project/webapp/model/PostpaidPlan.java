/**
 * 
 */
package com.project.webapp.model;

/**
 * @author loire
 *
 */
public class PostpaidPlan {

	private long id;
	private String description;
	private double msf;
	private int contractPeriod;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getMsf() {
		return msf;
	}
	public void setMsf(double msf) {
		this.msf = msf;
	}
	
	public int getContractPeriod() {
		return contractPeriod;
	}
	public void setContractPeriod(int contractPeriod) {
		this.contractPeriod = contractPeriod;
	}
}