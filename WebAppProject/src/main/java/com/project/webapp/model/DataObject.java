package com.project.webapp.model;

public class DataObject {

	private Object data;

	public DataObject(Object data) {
		super();
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
