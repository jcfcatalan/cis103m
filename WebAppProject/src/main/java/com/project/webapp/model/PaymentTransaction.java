/**
 * 
 */
package com.project.webapp.model;

/**
 * @author loire
 *
 */
public class PaymentTransaction {

	private long id;
	private long accountNo;
	private long postpaidNo;
	private String transactionDate;
	private double amount;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public long getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}
	
	public long getPostpaidNo() {
		return postpaidNo;
	}
	public void setPostpaidNo(long postpaidNo) {
		this.postpaidNo = postpaidNo;
	}
	
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}