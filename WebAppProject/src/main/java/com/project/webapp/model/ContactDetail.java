/**
 * 
 */
package com.project.webapp.model;

/**
 * @author loire
 *
 */
public class ContactDetail {

	private String contactNo;
	private int subscriberId;
	private String type;
	
	public ContactDetail(String contactNo, String type){
		this.contactNo = contactNo;
		this.type = type;
	}
	
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	public int getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(int subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}