/**
 * 
 */
package com.project.webapp.model;

/**
 * @author loire
 *
 */
public class RequestData {

	private long id;
	private long requestId;
	private String newStreet;
	private String newCity;
	private String newProvince;
	private String newZipCode;
	private int newCreditLimit;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	
	public String getNewStreet() {
		return newStreet;
	}
	public void setNewStreet(String newStreet) {
		this.newStreet = newStreet;
	}
	
	public String getNewCity() {
		return newCity;
	}
	public void setNewCity(String newCity) {
		this.newCity = newCity;
	}
	
	public String getNewProvince() {
		return newProvince;
	}
	public void setNewProvince(String newProvince) {
		this.newProvince = newProvince;
	}
	
	public String getNewZipCode() {
		return newZipCode;
	}
	public void setNewZipCode(String newZipCode) {
		this.newZipCode = newZipCode;
	}
	
	public int getNewCreditLimit() {
		return newCreditLimit;
	}
	public void setNewCreditLimit(int newCreditLimit) {
		this.newCreditLimit = newCreditLimit;
	}
}