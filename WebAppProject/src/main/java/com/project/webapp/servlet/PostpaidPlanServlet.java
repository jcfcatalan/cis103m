package com.project.webapp.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.impl.jc.PostpaidPlanDao;
import com.project.webapp.dao.impl.jc.PostpaidPlanDaoImpl;
import com.project.webapp.model.PostpaidPlan;
import com.project.webapp.util.JSONFormatter;

public class PostpaidPlanServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8804583364386132065L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONFormatter jsonFormatter = new JSONFormatter();
		PostpaidPlanDao planDao = new PostpaidPlanDaoImpl();
		List<PostpaidPlan> plans = planDao.getAllPostpaidPlans();
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonFormatter.toJsonString(plans));

	}
	
}
