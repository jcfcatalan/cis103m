package com.project.webapp.servlet.approver;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.Approver;
import com.project.webapp.model.SubscriberReport;

public class Welcome extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2756770815167775422L;
	
	private static final String VIEW = "/views/approver/container.jsp";	
	
	private static final String sqlQuery_SelectApprover = "SELECT * FROM APPROVER WHERE EMAILADDRESS = ?;";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(VIEW).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getAttribute("username").toString();
		
		Approver approver = retrieveApproverDetails(username);
		List<SubscriberReport> subscribers = getAllSubscriber();
		request.setAttribute("approverId", approver.getId());
		request.setAttribute("approverName", approver.getLastName() + ", " + approver.getFirstName());
		request.setAttribute("approverEmail", approver.getEmailAddress());
		request.setAttribute("subscribers", subscribers);
		request.getRequestDispatcher(VIEW).forward(request, response);
	}
	
	private Approver retrieveApproverDetails(String username){
		Approver approver = null;
		SQLDao sqlDao = new SQLDao();
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_SelectApprover);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				approver = new Approver();
				approver.setId(rs.getLong("id"));
				approver.setFirstName(rs.getString("firstName").toUpperCase());
				approver.setLastName(rs.getString("lastName").toUpperCase());
				approver.setEmailAddress(rs.getString("emailAddress").toUpperCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return approver;
	}
	
	private List<SubscriberReport> getAllSubscriber(){
		List<SubscriberReport> subscribers = new ArrayList<SubscriberReport>();
		
		String select1 = "SELECT s.*, pa.accountNo, pa.postpaidNo, pa.accountStatus, p.description, p.id as planId " 
				+ "FROM subscriber as s, postpaid_account as pa, postpaid_plan as p WHERE s.id = pa.subscriberId and p.id = pa.planId;";
		
		SQLDao sqlDao = new SQLDao();
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(select1);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				SubscriberReport subscriber = new SubscriberReport();
				subscriber.setId(rs.getLong("id"));
				subscriber.setFirstName(rs.getString("firstName").toUpperCase());
				subscriber.setMiddleName(rs.getString("middleName").toUpperCase());
				subscriber.setLastName(rs.getString("lastName").toUpperCase());
				subscriber.setStreet(rs.getString("street").toUpperCase());
				subscriber.setCity(rs.getString("city").toUpperCase());
				subscriber.setProvince(rs.getString("province").toUpperCase());
				subscriber.setZipCode(rs.getInt("zipCode"));
				subscriber.setAccountNo(rs.getLong("accountNo"));
				subscriber.setMobileNo(rs.getString("postpaidNo"));
				subscriber.setAccountStatus(rs.getString("accountStatus").toUpperCase());
				subscriber.setPlanDescription(rs.getString("description").toUpperCase());
				subscriber.setPlanId(rs.getInt("planId"));
				subscribers.add(subscriber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return subscribers;
	}
}