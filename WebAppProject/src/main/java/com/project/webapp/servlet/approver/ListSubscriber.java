package com.project.webapp.servlet.approver;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.impl.jc.SubscriberDao;
import com.project.webapp.dao.impl.jc.SubscriberDaoImpl;
import com.project.webapp.model.DataObject;
import com.project.webapp.model.SubscriberReport;
import com.project.webapp.util.JSONFormatter;

public class ListSubscriber extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6258637946791771630L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONFormatter jsonFormatter = new JSONFormatter();
		SubscriberDao subscriberDao = new SubscriberDaoImpl();
		List<SubscriberReport> sr = subscriberDao.getSubscriberReport();
		DataObject data = new DataObject(sr);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonFormatter.toJsonString(data));
	}

}
