package com.project.webapp.servlet.approver;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.ChangeBilling;
import com.project.webapp.model.DataObject;
import com.project.webapp.util.JSONFormatter;

public class ChangeBillingRequest extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6543446022173652791L;
	
	private static final String sqlQuery_selectChangeBilling = "select sr.subscriberId," +
												"sr.accountNo, sr.description, sr.requestedDate, sr.requestStatus," +
												"srd.requestId, srd.newStreet, srd.newCity, srd.newProvince, srd.newZipCode" +
												"from service_request as sr, service_request_data as srd" +
												"where sr.id = srd.requestId" +
												"and sr.description = 'Change Billing Address'" +
												"and sr.requestStatus = 'P';";
	
	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		JSONFormatter jsonFormatter = new JSONFormatter();
		List<ChangeBilling> list = getBillingRequest();
		DataObject data = new DataObject(list);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonFormatter.toJsonString(data));
	}
	
	private List<ChangeBilling> getBillingRequest(){
		List<ChangeBilling> list = new ArrayList<ChangeBilling>();
		SQLDao sqlDao = new SQLDao();
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_selectChangeBilling);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				ChangeBilling cb = new ChangeBilling();
				cb.setSubscriberId(rs.getLong("subscriberId"));
				cb.setAccountNo(rs.getLong("accountNo"));
				cb.setRequestId(rs.getLong("requestId"));
				cb.setDescription(rs.getString("description"));
				cb.setRequestedDate(rs.getString("requestedDate"));
				cb.setStatus(rs.getString("requestStatus"));
				cb.setStreet(rs.getString("newStreet"));
				cb.setCity(rs.getString("newCity"));
				cb.setProvince(rs.getString("newProvince"));
				cb.setZipCode(rs.getInt("newZipCode"));
				list.add(cb);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return list;
	}
}