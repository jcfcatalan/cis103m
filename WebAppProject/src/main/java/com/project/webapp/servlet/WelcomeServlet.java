package com.project.webapp.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.model.PostpaidPlan;
import com.project.webapp.model.Subscriber;
import com.project.webapp.util.Validator;

public class WelcomeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8874800753687998406L;
	
	private static DecimalFormat df = new DecimalFormat("#,##0.00");
	
	private static final String VIEW = "/views/subscriber/container.jsp";	
	
	private static final String sqlQuery_SelectPlan = "SELECT PLAN.*, ACCOUNT.* FROM POSTPAID_PLAN AS PLAN, POSTPAID_ACCOUNT AS ACCOUNT "
													+ "WHERE PLAN.ID = ACCOUNT.PLANID AND ACCOUNT.ACCOUNTNO = ?;";
	private static final String sqlQuery_SelectSubscriber = "SELECT SUB.*, ACCOUNT.* FROM SUBSCRIBER AS SUB, POSTPAID_ACCOUNT AS ACCOUNT "
													+ "WHERE SUB.ID = ACCOUNT.SUBSCRIBERID AND ACCOUNT.ACCOUNTNO = ?;";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(VIEW).forward(request, response);
	}

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PostpaidAccount postpaidAccount = retrieveAccountDetails(request.getAttribute("mobileNo").toString());
		PostpaidPlan postpaidPlan = retrievePlanDetails(postpaidAccount.getAccountNo());
		Subscriber subscriber = retrieveSubscriberDetails(postpaidAccount.getAccountNo());
				
		//2015-10-06
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime dt = formatter.parseDateTime(postpaidAccount.getActivationDate());
		DateTime contractEnd = dt.plusMonths(postpaidPlan.getContractPeriod());
		
		request.setAttribute("accountNo", String.valueOf(postpaidAccount.getAccountNo()));
		request.setAttribute("subscriberId", String.valueOf(postpaidAccount.getSubscriberId()));
		request.setAttribute("subscriberName", subscriber.getFirstName() + " " + subscriber.getLastName());
		request.setAttribute("subscriberAddress", subscriber.getStreet() + " " + subscriber.getCity() + " " + subscriber.getProvince() + " " + subscriber.getZipCode());
		request.setAttribute("subscriberEmail", subscriber.getEmailAddress());
		request.setAttribute("planId", String.valueOf(postpaidAccount.getPlanId()));
		request.setAttribute("planDescription", postpaidPlan.getDescription());
		request.setAttribute("planCreditLimit", df.format(postpaidAccount.getCreditLimit()));
		request.setAttribute("planContractDuration", postpaidPlan.getContractPeriod());
		request.setAttribute("planActivationDate", postpaidAccount.getActivationDate());
		request.setAttribute("planContractEnd", contractEnd.toYearMonthDay());
		request.setAttribute("planBalance", df.format(postpaidAccount.getBalance()));
		request.setAttribute("planDetails", postpaidPlan);
		request.getRequestDispatcher(VIEW).forward(request, response);
	}
	
	private PostpaidAccount retrieveAccountDetails(String postpaidNo){
		Validator validator = new Validator();
		PostpaidAccount postpaidAccount = validator.checkPostpaidAccount(postpaidNo);
		return postpaidAccount;
	}
	
	private PostpaidPlan retrievePlanDetails(long accountNo){
		SQLDao sqlDao = new SQLDao();
		PostpaidPlan postpaidPlan = null;
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_SelectPlan);
			ps.setLong(1, accountNo);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				postpaidPlan = new PostpaidPlan();
				postpaidPlan.setId(rs.getLong("id"));
				postpaidPlan.setDescription(rs.getString("description"));
				postpaidPlan.setMsf(rs.getDouble("msf"));
				postpaidPlan.setContractPeriod(rs.getInt("contractPeriod"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return postpaidPlan;
	}
	
	private Subscriber retrieveSubscriberDetails(long accountNo){
		SQLDao sqlDao = new SQLDao();
		Subscriber subscriber = null;
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_SelectSubscriber);
			ps.setLong(1, accountNo);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				subscriber = new Subscriber();
				subscriber.setId(rs.getLong("id"));
				subscriber.setFirstName(rs.getString("firstName").toUpperCase());
				subscriber.setLastName(rs.getString("lastName").toUpperCase());
				subscriber.setStreet(rs.getString("street").toUpperCase());
				subscriber.setCity(rs.getString("city").toUpperCase());
				subscriber.setProvince(rs.getString("province").toUpperCase());
				subscriber.setZipCode(rs.getInt("zipCode"));
				subscriber.setEmailAddress(rs.getString("emailAddress").toUpperCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return subscriber;
	}
}