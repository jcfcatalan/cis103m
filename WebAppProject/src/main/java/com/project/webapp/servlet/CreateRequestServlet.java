package com.project.webapp.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.model.PostpaidPlan;
import com.project.webapp.util.JSONFormatter;
import com.project.webapp.util.ResponseFormatter;
import com.project.webapp.util.Validator;

public class CreateRequestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6159296561830197709L;
	
	//private static final String MESSAGE = "message";
	private static final String NEWLIMIT = "newCreditLimit";
	private static final String MOBILENO = "mobileNo";
	private static final String ACCOUNTNO = "accountNo";
	private static final String SUBSCRIBERID = "subscriberId";
	//private static final String CREDITLIMIT_VIEW = "/views/subscriber/changeCreditLimit.jsp";
	//private static final String BILLING_VIEW = "/views/subscriber/changeBilling.jsp";

	private static final String sqlQuery_SelectPlan = "SELECT * FROM POSTPAID_PLAN WHERE ID=?;";
	private static final String sqlQuery_InsertRequest = "INSERT INTO SERVICE_REQUEST " 
								+ "(ID, SUBSCRIBERID, ACCOUNTNO, DESCRIPTION, REQUESTEDDATE, REQUESTSTATUS, APPROVERID, MODIFICATIONDATE) "
								+ "VALUES (?, ?, ?, ?, NOW(), ?, ?, ?); ";
	private static final String sqlQuery_InsertRequestData = "INSERT INTO SERVICE_REQUEST_DATA " 
								+ "(ID, REQUESTID, NEWSTREET, NEWCITY, NEWPROVINCE, NEWZIPCODE, NEWCREDITLIMIT) "
								+ "VALUES (?, ?, ?, ?, ?, ?, ?);";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONFormatter jsonFormatter = new JSONFormatter();
		JSONObject jsonObject = jsonFormatter.createJsonObject(request);
		int requestType = jsonObject.getInt("requestType");
		if(requestType == 1){
			changeBillingAddress(jsonObject, request, response);
		}else if(requestType == 2){
			changeCreditLimit(jsonObject, request, response);
		}
		
		
		/*String creditLimit = jsonObject.getString(NEWLIMIT);
		if(validator.isNumeric(creditLimit)){
			String mobileNo = jsonObject.getString(MOBILENO);
			int newCreditLimit = jsonObject.getInt(NEWLIMIT);
			
			PostpaidAccount postpaidAccount = validator.checkPostpaidAccount(mobileNo);
			if(postpaidAccount != null){
				PostpaidPlan postpaidPlan = getPlanDetails(postpaidAccount.getPlanId()); 
				if(newCreditLimit >= postpaidPlan.getMsf()){
					boolean success = submitRequest(postpaidAccount.getAccountNo(), postpaidAccount.getSubscriberId(), 
							postpaidAccount.getPlanId(), newCreditLimit);
					if(success){
						message = "Your request has been saved.";
					}else{
						message = "Request cannot be saved.";
					}
				}else{
					message = "Credit must not be lower than your monthly service fee.";
				}
				
				request.setAttribute(MESSAGE, message);
				request.getRequestDispatcher("/views/subscriber/changeCreditLimit.jsp").forward(request, response);
			}			
		}*/
	}
	
	private void changeBillingAddress(JSONObject jsonObject, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ResponseFormatter responseFormatter = new ResponseFormatter();
		String	message = "",
				subscriberId = jsonObject.getString(SUBSCRIBERID),
				accountNo = jsonObject.getString(ACCOUNTNO),
				newStreet = jsonObject.getString("newStreet"),
				newCity = jsonObject.getString("newCity"),
				newProvince = jsonObject.getString("newProvince"),
				newZipCode = jsonObject.getString("newZipCode");
		String[] address = {newStreet, newCity, newProvince, newZipCode};
		boolean success = submitRequest(Long.parseLong(subscriberId), Long.parseLong(accountNo), "Change Billing Address", address, 0);
		if(success){
			message = "Your request has been saved.";
			response = responseFormatter.constructResponse(response, message, success);
		}else{
			message = "Error occured. Request cannot be saved. Please try again";
			response = responseFormatter.constructResponse(response, message, success);
		}

		//request.setAttribute(MESSAGE, message);
		//request.getRequestDispatcher(BILLING_VIEW).forward(request, response);
	}
	
	/*private boolean submitRequestBilling(String subscriberId, String accountNo, String street, String city, String province, String zipCode){
		boolean success = false;
		SQLDao sqlDao = new SQLDao();
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_InsertRequest, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, null);
			ps.setLong(2, Long.parseLong(subscriberId));
			ps.setLong(3, Long.parseLong(accountNo));
			ps.setString(4, "Change Billing Address");
			ps.setString(5, "GETDATE()");
			ps.setString(6, "P");
			ps.setString(7, null);
			ps.setString(8, null);
						
			int requestId = ps.executeUpdate();
			if(requestId != 0){
				success = storeRequestDetails(requestId, street, city, province, zipCode, 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}*/
	
	private void changeCreditLimit(JSONObject jsonObject, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ResponseFormatter responseFormatter = new ResponseFormatter();
		Validator validator = new Validator();
		String creditLimit = jsonObject.getString(NEWLIMIT);
		String message = "";
		
		if(validator.isNumeric(creditLimit)){
			String mobileNo = jsonObject.getString(MOBILENO);
			int newCreditLimit = jsonObject.getInt(NEWLIMIT);
			
			PostpaidAccount postpaidAccount = validator.checkPostpaidAccount(mobileNo);
			if(postpaidAccount != null){
				PostpaidPlan postpaidPlan = getPlanDetails(postpaidAccount.getPlanId()); 
				if(newCreditLimit >= postpaidPlan.getMsf()){
					boolean success = submitRequest(postpaidAccount.getSubscriberId(), postpaidAccount.getAccountNo(), "Change Credit Limit", null, newCreditLimit);
					if(success){
						message = "Your request has been saved.";
						response = responseFormatter.constructResponse(response, message, success);
					}else{
						message = "Error occured. Request cannot be saved. Please try again";
						response = responseFormatter.constructResponse(response, message, success);
					}
				}else{
					message = "Credit must not be lower than your monthly service fee.";
					response = responseFormatter.constructResponse(response, message, false);
				}
				
				//request.setAttribute(MESSAGE, message);
				//request.getRequestDispatcher(CREDITLIMIT_VIEW).forward(request, response);
			}			
		}
	}
	
	private PostpaidPlan getPlanDetails(long planId){
		SQLDao sqlDao = new SQLDao();
		PostpaidPlan plan = null;
		try {
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_SelectPlan);
			ps.setLong(1, planId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				plan = new PostpaidPlan();
				plan.setId(rs.getLong("id"));
				plan.setDescription(rs.getString("description"));
				plan.setMsf(rs.getInt("msf"));
				plan.setContractPeriod(rs.getInt("contractPeriod"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return plan;
	}
	
	/*private boolean submitRequestCreditLimit(long accountNo, long subscriberId, long planId, int creditLimit){
		boolean success = false;
		
		//initializing dao class
		SQLDao sqlDao = new SQLDao();
		
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_InsertRequest, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, null);
			ps.setLong(2, subscriberId);
			ps.setLong(3, accountNo);
			ps.setString(4, "Change Credit Limit");
			ps.setString(5, "GETDATE()");
			ps.setString(6, "P");
			ps.setString(7, null);
			ps.setString(8, null);
						
			int requestId = ps.executeUpdate();
			if(requestId != 0){
				success = storeRequestDetails(requestId, null, null, null, null, creditLimit);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}*/
	
	private boolean submitRequest(long subscriberId, long accountNo, String description, String[] address, int creditLimit){
		//initializing dao class
		SQLDao sqlDao = new SQLDao();
		boolean success = false;
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_InsertRequest, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, null);
			ps.setLong(2, subscriberId);
			ps.setLong(3, accountNo);
			ps.setString(4, description);
			ps.setString(5, "P");
			ps.setString(6, null);
			ps.setString(7, null);
			ps.executeUpdate();
			
			PreparedStatement p = con.prepareStatement("SELECT LAST_INSERT_ID();");
			ResultSet rs = p.executeQuery();
			int requestId = 0;
			while(rs.next()){
				requestId = rs.getInt(1);
			}
			if(requestId != 0){
				if(address != null){
					success = storeRequestDetails(requestId, address[0], address[1], address[2], address[3], creditLimit);
				}else{
					success = storeRequestDetails(requestId, null, null, null, null, creditLimit);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}
	
	private boolean storeRequestDetails(int requestId, String street, String city, String province, String zipCode, int creditLimit){
		//initializing dao class
		SQLDao sqlDao = new SQLDao();
		boolean success = false;
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			PreparedStatement ps = con.prepareStatement(sqlQuery_InsertRequestData);
			ps.setString(1, null);
			ps.setInt(2, requestId);
			ps.setString(3, street);
			ps.setString(4, city);
			ps.setString(5, province);
			ps.setInt(6, zipCode==null ? 0 : Integer.valueOf(zipCode));
			ps.setInt(7, creditLimit==0 ? 0 : creditLimit);
			
			int row = ps.executeUpdate();
			if(row != 0){
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}
}