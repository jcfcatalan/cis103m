package com.project.webapp.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.util.Validator;

public class CreateAccountServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7198430466236716222L;
	
	private PostpaidAccount postpaidAccount = new PostpaidAccount();
	
	private static final String MESSAGE = "message";
	private static final String MOBILE = "mobileNo";
	private static final String FNAME = "firstName";
	private static final String LNAME = "lastName";
	private static final String PASSWORD = "password";
	private static final String CONFIRMPASSWORD = "confirmPassword";
	private static final String VIEW = "/views/main/register.jsp";
	
	//private static final String sqlQuery_updatePostpaidAccount = "UPDATE POSTPAID_ACCOUNT SET PASSWORD=? WHERE POSTPAIDNO=?";
	private static final String sqlQuery_selectLogin = "SELECT USERNAME FROM LOGIN WHERE USERNAME = ?;";
	private static final String sqlQuery_insertLogin = "INSERT INTO LOGIN " + 
								"(ID, FIRSTNAME, LASTNAME, USERNAME, PASSWORD, SUBSCRIBERID, APPROVERID) VALUES (?, ?, ?, ?, ?, ?, ?);";	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(VIEW).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dispatcher = "", message = "";
        
		//reading the user input
        String postpaidNo = request.getParameter(MOBILE);
        String firstName = request.getParameter(FNAME);
        String lastName = request.getParameter(LNAME);
        String password = request.getParameter(PASSWORD);
        String confirmPassword = request.getParameter(CONFIRMPASSWORD);
        
        //check postpaid mobile number
        String ppMsg = checkPostpaidNumber(postpaidNo);
        if(!ppMsg.isEmpty()){
        	request.setAttribute(MESSAGE, ppMsg);
            request.getRequestDispatcher(VIEW).forward(request, response);
            return;
        }
        
        //check password
        String pwMsg = checkPasswordFields(password, confirmPassword);
        if(!pwMsg.isEmpty()){
        	request.setAttribute(MESSAGE, pwMsg);
            request.getRequestDispatcher(VIEW).forward(request, response);
            return;
        }
        
        boolean success = register(postpaidNo, password, postpaidAccount.getSubscriberId(), 0, firstName, lastName);
    	if(success){
    		message = "Account has been created. Please login to view your account details.";
        	dispatcher = "/login";
    	}else{
    		message = "Account cannot be created.";
        	dispatcher = "/views/main/register.jsp";
    	}

        request.setAttribute(MESSAGE, message);
        request.getRequestDispatcher(dispatcher).forward(request, response);
	}
	
	private String checkPostpaidNumber(String postpaidNumber){
		Validator validator = new Validator();
		String msg = "";
		
		if(validator.isNumeric(postpaidNumber)){
			//PostpaidAccount pa = validator.checkPostpaidAccount(postpaidNumber);
			postpaidAccount = validator.checkPostpaidAccount(postpaidNumber);
			if(postpaidAccount != null){
				/*if(postpaidAccount.getAccountStatus().equals("A") && postpaidAccount.getPassword() != null){
					msg = "An account is already existing for the mobile number.";
				}*/
				if(postpaidAccount.getAccountStatus().equals("A")){
					if(checkLogin(postpaidNumber)){
						msg = "An account is already existing for the mobile number.";
					}
				}
			}else{
				msg = "Invalid mobile number.";
			}
		}else{
			msg = "Invalid mobile number.";
		}
		
		/*if(isNumeric(postpaidNumber)){
			if(postpaidNumber.length() == 11){
				if(validator.checkPostpaidAccount(postpaidNumber)){
		            if(!validator.checkPostpaidNo(postpaidNumber)){
		            	msg = "An account is already existing for the mobile number.";
		            }
		        }
			}
		}else{
        	msg = "Invalid mobile number.";
        }*/
		
		return msg;
	}
	
	private String checkPasswordFields(String password, String confirmPassword){
		String msg = "";
		if(password != null && confirmPassword != null){
			if(password.length() >= 6){
	            if(!password.equals(confirmPassword)){
	            	msg = "Password did not match.";
	            } 
			}else{
				msg = "Password must be at least 6 characters long.";
			}
        }else{
        	msg = "Missing requires field/s.";
        }
		return msg;
	}
	
	private boolean checkLogin(String username){
		boolean success = false;
		
		SQLDao sqlDao = new SQLDao();
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			/*PreparedStatement ps = con.prepareStatement(sqlQuery_updatePostpaidAccount);
			ps.setString(1, password);
			ps.setString(2, postpaidNo);*/
			
			PreparedStatement ps = con.prepareStatement(sqlQuery_selectLogin);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			success = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}
	
	private boolean register(String postpaidNo, String password, long subscriberId, long approverId, String fname, String lname){
		boolean success = false;
		SQLDao sqlDao = new SQLDao();
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			/*PreparedStatement ps = con.prepareStatement(sqlQuery_updatePostpaidAccount);
			ps.setString(1, password);
			ps.setString(2, postpaidNo);*/
			
			PreparedStatement ps = con.prepareStatement(sqlQuery_insertLogin);
			ps.setString(1, null);
			ps.setString(2, fname);
			ps.setString(3, lname);
			ps.setString(4, postpaidNo);
			ps.setString(5, password);
			ps.setLong(6, subscriberId==0 ? 0 : subscriberId);
			ps.setLong(7, approverId==0 ? 0 : approverId);
			
			int row = ps.executeUpdate();
			if(row != 0){
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}
}