package com.project.webapp.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.webapp.dao.SQLDao;
import com.project.webapp.model.MessageResponse;
import com.project.webapp.model.PostpaidAccount;
import com.project.webapp.util.JSONFormatter;
import com.project.webapp.util.Validator;

public class PaymentServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5373871647379737706L;

	private static DecimalFormat df = new DecimalFormat("#,##0.00");
	
	private static final String MOBILENO = "mobileNo";
	private static final String AMOUNT_PAID = "paidAmount";
	private static final String ACCOUNTNO = "accountNo";
	
	private static final String sqlQuery_UpdateBalance = "UPDATE POSTPAID_ACCOUNT SET BALANCE = ?, ACCOUNTSTATUS = ? WHERE ACCOUNTNO = ?;";
	private static final String sqlQuery_InsertTransaction = "INSERT INTO PAYMENT_TRANSACTION (ID, POSTPAIDNO, ACCOUNTNO, TRANSACTIONDATE, AMOUNT) VALUES (?, ?, ?, NOW(), ?);";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException{
		JSONFormatter jsonFormatter = new JSONFormatter();
		JSONObject jsonObject = jsonFormatter.createJsonObject(request);
		String postpaidNo = "0" + String.valueOf(jsonObject.getLong(MOBILENO));
		int paidAmount = Integer.valueOf(jsonObject.getString(AMOUNT_PAID));
		int accountNo = jsonObject.getInt(ACCOUNTNO);
		
		System.out.println(postpaidNo);
		
		Validator validator = new Validator();
		PostpaidAccount postpaidAccount = validator.checkPostpaidAccount(postpaidNo);
		
		double accountBalance = postpaidAccount.getBalance();
		double newAccountBalance = accountBalance - paidAmount;
		
		boolean success = createTransaction(postpaidNo, accountNo, paidAmount, newAccountBalance);
		if(success){
			double newAmountDue = returnAmountDue(postpaidNo);
		    response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(jsonFormatter.toJsonString(new MessageResponse(df.format(newAmountDue), success)));
		}else{
		    response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(jsonFormatter.toJsonString(new MessageResponse("Error occured. Please try again", success)));
		}
	}
	
	private boolean createTransaction(String postpaidNo, int accountNo, double paidAmount, double newAccountBalance){
		//initializing dao class
		SQLDao sqlDao = new SQLDao();
		boolean success = false;
		try {
			//creating connection with the database
			Connection con = sqlDao.establishConnection();
			
			//insert to transaction table
			PreparedStatement ps = con.prepareStatement(sqlQuery_InsertTransaction);
			ps.setString(1, null);
			ps.setString(2, postpaidNo);
			ps.setLong(3, accountNo);
			ps.setDouble(4, paidAmount);
			ps.executeUpdate();
			
			//update account balance and status
			PreparedStatement p = con.prepareStatement(sqlQuery_UpdateBalance);
			p.setInt(1, (int) newAccountBalance);
			p.setString(2, "A");
			p.setLong(3, accountNo);
			int row = p.executeUpdate(); 
			if(row != 0){
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlDao.closeConnection();
		}
		
		return success;
	}
	
	private double returnAmountDue(String postpaidNo){
		Validator validator = new Validator();
		PostpaidAccount account = validator.checkPostpaidAccount(postpaidNo);
		return account.getBalance();
	}
}