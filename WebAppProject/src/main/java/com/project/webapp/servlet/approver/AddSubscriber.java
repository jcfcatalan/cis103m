package com.project.webapp.servlet.approver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.project.webapp.dao.impl.jc.SubscriberDao;
import com.project.webapp.dao.impl.jc.SubscriberDaoImpl;
import com.project.webapp.model.ContactDetail;
import com.project.webapp.model.MessageResponse;
import com.project.webapp.model.Subscriber;
import com.project.webapp.util.JSONFormatter;

public class AddSubscriber extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2611809116521443798L;
	
	private static final String VIEW = "/views/approver/addSubscriber.jsp";	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher(VIEW).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		JSONFormatter jsonFormatter = new JSONFormatter();
		JSONObject jsonObject = jsonFormatter.createJsonObject(request);
		SubscriberDao subDao =  new SubscriberDaoImpl();
		
		List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
		Subscriber subscriber = new Subscriber();
		subscriber.setFirstName(jsonObject.getString("firstName"));
		subscriber.setMiddleName(jsonObject.getString("middleName"));
		subscriber.setLastName(jsonObject.getString("lastName"));
		subscriber.setStreet(jsonObject.getString("street"));
		subscriber.setCity(jsonObject.getString("city"));
		subscriber.setProvince(jsonObject.getString("province"));
		subscriber.setZipCode(jsonObject.getInt("zipCode"));
		subscriber.setEmailAddress(jsonObject.getString("emailAddress"));
		subscriber.setMobileNo(jsonObject.getString("mobileNo"));
		subscriber.setTelephoneNo(jsonObject.getString("telephoneNo"));
		subscriber.setContactDetails(contactDetails);
		
		boolean success = subDao.add(subscriber, jsonObject.getInt("planId"));
		
		if(success){
		    response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(jsonFormatter.toJsonString(new MessageResponse("Successfully added to pending list", success)));
		}else{
		    response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(jsonFormatter.toJsonString(new MessageResponse("Error occured. Please try again", success)));
		}
	}
}