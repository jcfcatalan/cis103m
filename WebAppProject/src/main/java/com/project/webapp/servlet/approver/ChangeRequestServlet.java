package com.project.webapp.servlet.approver;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.webapp.dao.ApproverDao;
import com.project.webapp.dao.impl.ApproverDaoImpl;
import com.project.webapp.model.ApproverReport;
import com.project.webapp.model.DataObject;
import com.project.webapp.util.JSONFormatter;

public class ChangeRequestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4700802814642073189L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONFormatter jsonFormatter = new JSONFormatter();
		ApproverDao approverDao = new ApproverDaoImpl();
		List<ApproverReport> ar = approverDao.requestApprovalReport();
		DataObject data = new DataObject(ar);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(jsonFormatter.toJsonString(data));
	}

}
